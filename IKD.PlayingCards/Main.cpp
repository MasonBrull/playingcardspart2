// Playing Cards
// Izaac Dewilde
// Mason Brull

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum class Suit
{
	SPADES,
	DIAMONDS,
	HEARTS,
	CLUBS
};

enum class Rank
{
	TWO = 2,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE,
}; 

struct Card
{
	Rank rank;
	Suit suit;
};

// Prints the rank and suit of the card passed into it
void PrintCard(Card card)
{
	//Prints rank
	switch (card.rank)
	{
	case Rank::TWO: cout << "Two of ";
		break;
	case Rank::THREE: cout << "Three of ";
		break;
	case Rank::FOUR: cout << "Four of ";
		break;
	case Rank::FIVE: cout << "Five of ";
		break;
	case Rank::SIX: cout << "Six of ";
		break;
	case Rank::SEVEN: cout << "Seven of ";
		break;
	case Rank::EIGHT: cout << "Eight of ";
		break;
	case Rank::NINE: cout << "Nine of ";
		break;
	case Rank::TEN: cout << "Ten of ";
		break;
	case Rank::JACK: cout << "Jack of ";
		break;
	case Rank::KING: cout << "King of ";
		break;
	case Rank::QUEEN: cout << "Queen of ";
		break;
	case Rank::ACE: cout << "Ace of ";
		break;
	}

	// Prints suit
	switch (card.suit)
	{
		case Suit::SPADES: cout << "Spades";
			break;
		case Suit::DIAMONDS: cout << "Diamonds";
			break;
		case Suit::HEARTS: cout << "Hearts";
			break;
		case Suit::CLUBS: cout << "Clubs";
	}
}

// Picks higher card and returns it
Card HighCard(Card card1, Card card2)
{
	if (card1.rank > card2.rank) // Picks higher card
	{
		return card1;
	}
	else if (card1.rank < card2.rank)
	{
		return card2;
	}
	else // Picks higher suit
	{
		if (card1.suit < card2.suit)
		{
			return card1;
		}
		else if (card1.suit > card2.suit)
		{
			return card2;
		}
		else // First card wins by default :/
		{
			return card1;
		}
	}
}

int main()
{
	Card c1;
	c1.rank = Rank::ACE;
	c1.suit = Suit::DIAMONDS;

	Card c2;
	c2.rank = Rank::ACE;
	c2.suit = Suit::DIAMONDS;

	PrintCard(c1);
	HighCard(c1, c2);


	(void)_getch();
	return 0;
}